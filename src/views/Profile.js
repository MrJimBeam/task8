import React, { useState, useEffect } from "react";
import styles from "./Profile.module.css";

function Profile({ match }) {
  useEffect(() => {
    fetchCharacter();
  }, []);

  const [profile, setProfile] = useState({});

  const fetchCharacter = async () => {
    const fetchCharacter = await fetch(
      `https://rickandmortyapi.com/api/character/${match.params.id}`
    );
    const profile = await fetchCharacter.json();
    setProfile(profile);
  };

  return (
    <div>
      <h1 style={{ color: "#1B2A51" }}>{profile.name}</h1>
      <img src={profile.image} alt={profile.name} />
      <h4 style={{ color: "#FAE5B8" }}>Status: {profile.status}</h4>
      <h4 style={{ color: "#FAE5B8" }}>
        Starred in {profile.episode && profile.episode.length} episodes{" "}
      </h4>
    </div>
  );
}

export default Profile;
