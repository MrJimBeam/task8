import React, { Component } from "react";
import CharacterList from "../components/CharacterList/CharacterList.js";
import SearchBar from "../components/SearchBar/SearchBar.js";
class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filterString: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(props) {
    this.setState({
      filterString: props
    }); }
  render() {
    return (
      <div style={{ background: "#1b262c", textAlign: "center" }}>
        <h1 style={{ color: "#007bff", important: true }}>
          {" "}
          Rick &amp; Morty Character Library
        </h1>
        <SearchBar onChange={this.handleChange}></SearchBar>

        <CharacterList filterString={this.state.filterString}></CharacterList>
      </div>
    );
  }
}

export default Main;
