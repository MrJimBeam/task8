import React, { Component } from "react";

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      filterString: ""
    };
  }

  handleChange(e) {
    this.setState({
      filterString: e.target.value
    });
    this.props.onChange(e.target.value);
  }
  render() {
    return (
      <div style={{ "justify-content": "center" }} className="col-md-6">
        <input
          value={this.state.filterString}
          onChange={e => this.handleChange(e)}
          type="text"
          className="form-control"
          placeholder="Enter a character name from R&M Universe"
        />
      </div>
    );
  }
}

export default SearchBar;
