import React from "react";
import "./App.css";

import Main from "./views/Main.js";
import Profile from "./views/Profile.js";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App" style={{ textAlign: "center" }}>
        <Switch>
          <Route path="/" exact component={Main} />
          <Route path="/profile/:id" component={Profile} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
